#!/usr/bin/env python
from tkinter import *
import sys

#===========Commandos(Funciones)====#
def clear():#borrar forma 1 #sin usarse
    en.delete(0,END)#
    return

def borrar():#borrar forma 2
    global operador
    operador = ''
    entrada.set("")

def Bclick(numeros):#funcion que permite interactuar con los botones
    global operador
    operador = operador + str(numeros)
    entrada.set(operador)

def igualA():#funcion igual(=) 
    global operador
    igual=str(eval(operador))
    entrada.set(igual)
    operador = ''

def ventana2():
    root = Toplevel()
    root.title('PUSH')
    root.resizable(False,False)
    Button(root,bd=16,bg='light green',text='PUSH',font=('arial',20,'bold'),relief=FLAT,padx=20,pady=20).pack(side=TOP)
    root.mainloop()
    
#================MASTER=============#
root = Tk()
root.title('calculadora')
root.configure(bg='Dark Gray')
root.resizable(False,False)

#==========MAIN-FRAME===============#
frame = Frame(root)
frame.pack()
#====variables======================#
entrada = StringVar()#variabel del entry
operador = " "
#========Entrads de datos===========#
en = Entry(frame,textvariable=entrada,bd=20,insertwidth = 3,font=80)
en.pack(side=TOP)

#====Frame linea NO.1===============#
frame1 = Frame(root)
frame1.pack(side=TOP)
#===========Botones(linea 2)========#
bt1 = Button(frame1,text='1',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(1))
bt1.pack(side=LEFT)
bt2 = Button(frame1,text='2',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(2))
bt2.pack(side=LEFT)
bt3 = Button(frame1,text='3',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(3))
bt3.pack(side=LEFT)
bt4 = Button(frame1,text='4',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(4))
bt4.pack(side=LEFT)
#==========Frame Linea NO.2==========#
frame2 = Frame(root)
frame2.pack(side=TOP)
#==========Botones(linea 2)==========#
bt5 = Button(frame2,text='5',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(5))
bt5.pack(side=LEFT)
bt6 = Button(frame2,text='6',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(6))
bt6.pack(side=LEFT)
bt7 = Button(frame2,text='7',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(7))
bt7.pack(side=LEFT)
bt8 = Button(frame2,text='8',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(8))
bt8.pack(side=LEFT)
#==========Frame LInea NO.3==========#
frame3 = Frame(root)
frame3.pack(side=TOP)
#==========Botones(linea 3)==========#
bt9 = Button(frame3,text='9',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(9))
bt9.pack(side=LEFT)
bt0 = Button(frame3,text='0',bd=8,padx=16,pady=16,fg='black',command=lambda:Bclick(0))
bt0.pack(side=LEFT)
btc = Button(frame3,text='c',bd=8,padx=16,pady=16,fg='black',bg='#6898E5',command=borrar)#boton para limpiar pantalla
btc.pack(side=LEFT)
btm = Button(frame3,text='=',bd=8,padx=16,pady=16,fg='black',bg='green',command=igualA)#igual a (=)
btm.pack(side=LEFT)
#============Frame linea NO4.=========#
frame4 = Frame(root)
frame4.pack(side=TOP)
#==================Boton(linea4)======#
btmas = Button(frame4,text='+',bd=8,padx=16,pady=16,fg='black',bg='#F7B955',command=lambda:Bclick('+'))#suma
btmas.pack(side=LEFT)
btmos = Button(frame4,text='-',bd=8,padx=16,pady=16,fg='black',bg='#D284C0',command=lambda:Bclick('-'))#resta
btmos.pack(side=LEFT)
btpor = Button(frame4,text='*',bd=8,padx=16,pady=16,fg='black',bg='#FF2E2E',command=lambda:Bclick('*'))#multi
btpor.pack(side=LEFT)
btdiv = Button(frame4,text='/',bd=8,padx=16,pady=16,fg='black',bg='#0E836C',command=lambda:Bclick('/'))#div
btdiv.pack(side=LEFT)



#===salida=============#
salir = Button(root,text='salir',bg='red',font=('arial',10,'bold'),relief=RIDGE,bd=8,command=lambda root=root:quit(root))
salir.pack(fill=X)
#======tanda#2=========#
tda = Button(root,text='ventan 2',font=('arial',10,'bold'),relief=RIDGE,bg='green',bd=8,command=ventana2)
tda.pack(fill=X)

root.mainloop()





#=========================KK's================================================================================#
#insertwidth = Tamaño del cursor
#font = tamaño del texto
#font: Entry(root,etc........,font=x)  uso es en este caso
#fg = color de la fuente(texto)
#fg : uso en Botones, Button(root, etc.......,fg='un color')

